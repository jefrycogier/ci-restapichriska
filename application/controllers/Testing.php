<?php

// import library dari REST_Controller
require APPPATH . 'libraries/REST_Controller.php';

// extends class dari REST_Controller
class Testing extends REST_Controller{

  // constructor
  public function __construct(){
    parent::__construct();
  }

  public function index_get(){

    // testing response
    $response['status']=200;
    $response['error']=false;
    $response['message']='Testing sukses';

    // tampilkan response
    $this->response($response);

  }

  public function user_get(){

    // testing response
    $response['status']=200;
    $response['error']=false;
    $response['user']['username']='jefry';
    $response['user']['email']='jefrymiftakhul@gmail.com';
    $response['user']['detail']['full_name']='jefry miftakhul';

    //tampilkan response
    $this->response($response);

  }

}

?>
